//
//  MovieManager.swift
//  OMDB
//
//  Created by Leandro Ramos on 2/25/20.
//  Copyright © 2020 Black Beard Games. All rights reserved.
//

import Foundation

struct MovieManager {// wich will perform the networking
    
    let viewController = ViewController()
    
    let movieUrl = "https://www.omdbapi.com/?i=tt3896198&apikey=b6531970"
    let posterUrl = "https://m.media-amazon.com/images/M/MV5BNjM0NTc0NzItM2FlYS00YzEwLWE0YmUtNTA2ZWIzODc2OTgxXkEyXkFqcGdeQXVyNTgwNzIyNzg@._V1_SX300.jpg"
    
    func performMovieRequest(urlRequest: String) {// the entire url to be called
        if let url = URL(string: movieUrl) {// transform url into url object to be used in the request
            //            print(url)
            //create a url session
            let session = URLSession(configuration: .default)
            //givin the session a task
            let task = session.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    print(error!)
                    return
                }
                
                if let safeData = data {//encoding the data so we actually can see the data, safe data variable tha shows
                    self.parseJSON(movieData: safeData)//safeData as parameters
                }
            }
            task.resume()
        }
    }
    //parse json function
    func parseJSON(movieData: Data) {// with data as a parameter
        let decoder = JSONDecoder()
        //do try catch to handle errors of decoded json
        do {
        let decodedData = try decoder.decode(MovieData.self, from: movieData)//call the data for decode json. movie data comes from the parameter of the function, that is subclassed as data object
            print(decodedData.Title)
            let titleContent = decodedData.Title
            
            let movieModel = MovieModel(titleContent: titleContent)
            
        } catch {
            print(error)
        }
    }
    
//    func getPosterData(urlPosterRequest: String) {
//        if let url = URL(string: posterUrl) {
//            let session = URLSession(configuration: .default)
//            let task = session.dataTask(with: url) { (data, response, error) in
//                if error != nil {
//                    print(error!)
//                    return
//                }
//            }
//            task.resume()
//        }
//    }
//    
//    func downloadImage(fromUrl: String) {
//        print("download started")
//        self.getPosterData(urlPosterRequest: posterUrl)
//        print("download finish")
////        DispatchQueue.main.async() {
////            self.viewController.imageView.image
////        }
//    }
}
