//
//  ViewController.swift
//  OMDB
//
//  Created by Leandro Ramos on 2/25/20.
//  Copyright © 2020 Black Beard Games. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    lazy var imageView: UIImageView = {
       UIImageView()
    }()
    
    lazy var titleLabel: UILabel = {
        UILabel()
    }()
    
    let movieManager = MovieManager()// create a new manager to call the url with the api
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        movieManager.performMovieRequest(urlRequest: movieManager.movieUrl)// passing the url request
//        movieManager.getPosterData(urlPosterRequest: movieManager.posterUrl)
//        movieManager.downloadImage(fromUrl: movieManager.posterUrl)
        viewHierarchy()
        constraitsMaker()
        additionalComponents()
    }
    
    func viewHierarchy() {
        view.addSubview(imageView)
        view.addSubview(titleLabel)
    }
    
    func constraitsMaker() {
        imageView.snp.makeConstraints { (maker) in
            maker.top.bottom.leading.trailing.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { (maker) in
            maker.center.leading.trailing.equalToSuperview()
        }
    }
    
    func additionalComponents() {
        titleLabel.textColor = .black
    }
}

